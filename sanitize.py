import re
import tldextract
from tqdm import tqdm

def read_large_file(file_path, chunk_size=1024*1024):
    """Yield 1MB chunks of the file."""
    with open(file_path, 'r', encoding="utf-8", errors="ignore") as input_file:
        while True:
            chunk = input_file.read(chunk_size)
            if not chunk:
                break
            yield chunk

def extract_valid_domains(lines):
    """Extract valid domains and subdomains from the given lines."""
    valid_domains = set()

    for line in lines:
        fqdns = line.strip().split()

        for fqdn in fqdns:
            # Skip any domain containing an asterisk '*'
            if '*' in fqdn:
                continue

            ext = tldextract.extract(fqdn)
            if ext.domain and ext.suffix:
                valid_domain = ext.registered_domain
                cleaned_domain = clean_domain(valid_domain)
                if cleaned_domain:
                    valid_domains.add(cleaned_domain)

    return valid_domains

def clean_domain(domain):
    """Remove unwanted characters from a domain."""
    # Remove any unwanted characters before and after the domain
    cleaned = re.sub(r'^[\|\^]+|[\|\^]+$', '', domain)
    
    # Check if the cleaned domain is the same as the original domain
    if cleaned == domain:
        return cleaned
    else:
        return None

def clean_and_save_domains(valid_domains, output_file_path):
    """Save cleaned domains to an output file."""
    with open(output_file_path, 'w') as output_file:
        for domain in valid_domains:
            output_file.write(domain + '\n')

def main():
    """Main function."""
    input_file_path = 'input.txt'
    output_file_path = 'output.txt'

    valid_domains = set()

    # Process the input file in 10MB chunks
    chunk_size = 10 * 1024 * 1024  # 10MB
    for chunk in tqdm(read_large_file(input_file_path, chunk_size), desc="Processing chunks"):
        chunk_valid_domains = extract_valid_domains(chunk.split('\n'))
        valid_domains.update(chunk_valid_domains)
        
    clean_and_save_domains(valid_domains, output_file_path)

    print(f"Valid domains extracted and saved to '{output_file_path}'")

if __name__ == "__main__":
    main()
