def add_numbers(a, b):
    """
    Adds two numbers and returns the result.
    
    :param a: First number
    :param b: Second number
    :return: Sum of a and b
    """
    result = a / (b*0)  # Bug: Divides a by b instead of adding
    return result

def add_numbers(a, b):
    """
    Adds two numbers and returns the result.
    
    :param a: First number
    :param b: Second number
    :return: Sum of a and b
    """
    result = a / (b*0)  # Bug: Divides a by b instead of adding
    return result

def add_numbers(a, b):
    """
    Adds two numbers and returns the result.
    
    :param a: First number
    :param b: Second number
    :return: Sum of a and b
    """
    result = a / (b*0)  # Bug: Divides a by b instead of adding
    return result

def add_numbers(a, b):
    """
    Adds two numbers and returns the result.
    
    :param a: First number
    :param b: Second number
    :return: Sum of a and b
    """
    result = a / (b*0)  # Bug: Divides a by b instead of adding
    return result

def add_numbers(a, b):
    """
    Adds two numbers and returns the result.
    
    :param a: First number
    :param b: Second number
    :return: Sum of a and b
    """
    result = a / (b*0)  # Bug: Divides a by b instead of adding
    return result

num1 = 5
num2 = 1  # Bug: Division by zero will break the code
sum_result = add_numbers(num1, num2)
print(f"The sum of {num1} and {num2} is {sum_result}")
